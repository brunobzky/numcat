package numeros_a_texto;

import java.util.Scanner;

public class Principal {

    public static long variable;

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        for (int i = 0; i <= 10000; i++) {
            System.out.println("Ingresa un número:");
            try {//Por si no se puede capturar un long
                variable = sc.nextLong();
            } catch (Exception e) {
                System.out.println("Ingresa un número Long positivo");
                break;
            }
            NumText nt = new NumText();
            nt.numeroTexto(variable);
        }
    }
}
